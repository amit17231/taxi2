from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings
from dashboard import apis
from dashboard import views
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView



urlpatterns = [
    path('admin/', admin.site.urls),
    path('dashboard/', include('dashboard.urls')),
	path('savemap', apis.saveMap),
	path('getdata', apis.getMapData),
	path('deletemapshape', apis.deleteMapShapes),
	path('editprofile', apis.editProfile),
	path('appLogin',apis.appLogin),
	path('login',obtain_auth_token,name="login"),
	path('appLogout',apis.logout),
	path('appRegistration',apis.appRegistration),
	path('tip_detail',apis.tip_detail),
	path('driver_detail', apis.driver_detail),
	path('custom_headers',apis.custom_headers),
	path('editpic',views.Editprofilepic),
	path(r'api/token',TokenObtainPairView.as_view()),
	path('api/token/refresh/',TokenRefreshView.as_view()),
	path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
	path('ratings/', include('star_ratings.urls', namespace='ratings')),
	path('rideHistory',apis.rideHistory),
	path('logout',apis.logout),
	path('payment',apis.payment),
	path('CardSave',apis.CardSave),
	path('intialstatebooking',apis.intialstatebooking),
	path('Startstatebooking',apis.Startstatebooking),
	path('Cancelbooking',apis.Cancelbooking),
	path('Sendrequest',apis.Sendrequest),
	path('lostreport',apis.lostreport),
	path('Contactus',apis.Contactus),
	path('driverContact',apis.driverContact),
	path('rating',apis.rating),
	path('promocode',apis.promocode),
	path('FAqs',apis.FAqs),
	path('go_online',apis.go_online),
	path('go_offline',apis.go_offline),
	path('FoundLostItem', apis.FoundLostItem),
	path('driverpastrides', apis.driverpastrides),
	path('ridehisrory', apis.ridehisrory),
	path('driverlist', apis.driverlist),
	path('acceptride', apis.acceptride),
	path('rejectride', apis.rejectride),
	path('addtaxinumber', apis.addtaxinumber),
	path('starttrip', apis.starttrip),
	path('endtrip', apis.endtrip),
	path('messagesApi',apis.messagesApi),
	path('locationLatLong',apis.locationLatLong),

]   + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns +=  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
