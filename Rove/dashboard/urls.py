from django.urls import path, include
from dashboard import views
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView, TemplateView
from .views import *

app_name = 'dashboard'

urlpatterns = [
    path('home/', HomePage.as_view(), name='home'),
    path('login/', DashboardLogin.as_view(), name='login'),
    # path('add-user/', AddUser.as_view(), name='add-user'),
    path('logout/', Logout.as_view(), name='logout'),
    # path('game-users/', GameAppUsers.as_view(), name='game-users'),
    # path('add_game_use/r', AddGameUsers.as_view(), name="add_game_user"),
    # path('edit-game-users/<user_id>/', EditGameUsers.as_view(), name='edit_game_users'),
    # path('delete-game-user/<user_id>/', DeleteGameUser.as_view(), name='delete_game_user'),
    path('add_staff/', AddStaff.as_view(), name="add_staff"),
    path('edit_admin/<user_id>/', EditAdmin.as_view(), name="edit_admin"),
    path('delete_admin/<user_id>/', DeleteAdmin.as_view(), name="delete_admin"),
    # path('delete_staff/<user_id>/', DeleteStaffUser.as_view(), name="delete_staff"),
    # path('staff_list/', StaffUserList.as_view(), name="staff_list"),
    # path('edit_staff/<user_id>/', EditStaffUser.as_view(), name="edit_staff"),
    # path('role_list/', RoleList.as_view(), name="role_list"),
    # path('add_role/', AddRole.as_view(), name="add_role"),
    # path('edit_role/<group_id>/', EditRole.as_view(), name="edit_role"),
    # path('delete_role/<group_id>/', DeleteRole.as_view(), name="delete_role"),
	path('add_vechicle/', addVehicle.as_view(), name="add_vechicle"),
	path('vechicle/', vechicleListing.as_view(), name="vechicle_listing"),
	path('delete_vechicle/<id>', vehicleDelete.as_view(), name="vechicle_delete"),
	path('edit_vechicle/', vehicleEdit.as_view(), name="vechicle_edit"),
	
	path('add_promocode/', addPromocode.as_view(), name="add_promocode"),
	path('promocode/', promocodeListing.as_view(), name="promocode_listing"),
	path('delete_promocode/<id>', promocodeDelete.as_view(), name="promocode_delete"),
	path('edit_promocode/', promocodeEdit.as_view(), name="promocode_edit"),
	
	path('add_driver/', addDriver.as_view(), name="add_driver"),
	path('driver/', driverListing.as_view(), name="driver_listing"),
	path('delete_driver/<id>', driverDelete.as_view(), name="driver_delete"),
	path('edit_driver/', driverEdit.as_view(), name="driver_edit"),
	
	path('add_zone/', addZone.as_view(), name="add_zone"),
	path('zone/', zoneListing.as_view(), name="zone_listing"),
	path('delete_zone/<id>', zoneDelete.as_view(), name="driver_delete"),
	path('edit_zone/', zoneEdit.as_view(), name="zone_edit"),
	path('change_password/', changePassword.as_view(), name="change_password"),
	
	path('user_management/', ManagementUsers.as_view(), name='user_management'),
	path('add_user/', AddUsers.as_view(), name='add-user'),
	path('delete_user/<id>', deleteUser.as_view(), name="delete_user"),
	path('edit_user/', editUser.as_view(), name='edit_user'),
	path('reset_password/<str:code>', ResetPassword.as_view(), name='reset_password'),
	path('forget_password/', ForgotPassword.as_view(), name='forget-password'),
	path('role/', roleList.as_view(), name="role_list"),
    path('add_role/', AddRole.as_view(), name="add_role"),
    path('edit_role/', EditRole.as_view(), name="edit_role"),
    path('delete_role/<id>', DeleteRole.as_view(), name="delete_role"),
	path(r'api-token-auth/', CustomAuthToken.as_view()),

	
	path('permission/', permissionList.as_view(), name="permission_list"),
	path('add_permission/', addPermission.as_view(), name="add_permission"),
	path('delete_permission/<id>', deletePermission.as_view(), name="delete_permission"),
	path('edit_permission/', editPermission.as_view(), name="edit_permission"),

	path('create/', create_session),
	path('access', access_session),
	path('delete', delete_session),

]