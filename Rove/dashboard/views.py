
from rest_framework.views import APIView
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth import login as auth_login
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic.base import View,TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

import datetime
from django.contrib.auth.hashers import check_password
from django.contrib.auth.hashers import make_password
from random import *
import random
from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives, EmailMessage
import json
from django.http import HttpResponse
from .models import Ride
# import StaffUserOnly
# import json
from .models import *
# Create your views here.

class StaffUserOnly(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.is_staff:
            pass
        else:
            messages.error(request, 'You don\'t Have Permission To Access this,Please Login With Right Credentials')
            return HttpResponseRedirect('/dashboard/login')
        return super().dispatch(request, *args, **kwargs)


class HomePage(StaffUserOnly, View):
	template_name = 'dashboard/user_list.html'
	def get(self, request):
		ride = Ride.objects.all()
		ridecomp=Ride.objects.filter(ridestatus='COMPLETED').count()
		ridecancelled=Ride.objects.filter(ridestatus='CANCELLED').count()
		usercount=User.objects.filter(is_active='1').count()
		return render(request, self.template_name, locals())


class DashboardLogin(View):
    template_name = 'dashboard/login.html'
    try:
        def get(self, request):
            if not request.user.is_authenticated:
                if request.COOKIES.get('username') is not None:
                    username = request.COOKIES.get('username')
                    password = request.COOKIES.get('password')

                return render(request, self.template_name, locals())
            else:

                return HttpResponseRedirect('/dashboard/home')

        def post(self, request):
            if not request.user.is_authenticated:
                username = request.POST.get("username")
                password = request.POST.get("password")
                if password and username:
                    user = authenticate(username=username, password=password)
                    if user is not None:
                        if user.is_staff == True:
                            auth_login(request, user)
                            print(request.POST.get('remember_me'))
                            if (request.POST.get('remember_me') == '1'):


                                messages.success(request, 'You are successfully logged in1 ')
                                html = HttpResponseRedirect('/dashboard/home')

                                html.set_cookie('username', request.POST.get("username"), max_age = None)
                                html.set_cookie('password', request.POST.get("password"), max_age = None)

                                return html
                            else:
                                messages.success(request, 'You are successfully logged in1 ')
                                html = HttpResponseRedirect('/dashboard/home')
                                return html

                        else:
                            messages.error(request, 'you are not authorised for this')
                    else:
                        messages.error(request, 'Invalid credentials')
                        return HttpResponseRedirect('/dashboard/login')
                return render(request, self.template_name, locals())
            else:
                return HttpResponseRedirect('/dashboard/home')
    except Exception as e:
        raise e


class Logout(View):
    def get(self, request):
        logout(request)
        messages.success(request, 'You are successfully logged out.')
        return redirect('/dashboard/login')


class AddStaff(StaffUserOnly, TemplateView):
    template_name = 'dashboard/add_users.html'

    def get(self, request, *args, **kwargs):
        group_list = Group.objects.all()
        model_list = ContentType.objects.exclude(
            app_label__in=["auth", "contenttypes", "sessions", "social_django", "admin"])
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        last_name = request.POST.get('last_name')
        password = request.POST.get('password')
        role = request.POST.get('role')
        status = request.POST.get('status')
        email = request.POST.get('email')
        selected_models = request.POST.getlist('models[]')
        email = email.lower()

        try:
            User.objects.get(email=email)
            messages.error(request, 'User already exists with given email.')
        except User.DoesNotExist:
            user = User.objects.create_user(
                email=email,
                username=email,
                first_name=name,
                last_name=last_name,


            )

            if role == 'super_admin':
                user.is_superuser = True
                user.groups.clear()
                user.is_staff = True
                if status == "A":
                    user.is_active = True
                else:
                    user.is_active = False
                user.set_password(password)
                user.save()
                permission_list = Permission.objects.all()
                user.user_permissions.set(permission_list)
                user.save()
                messages.success(request, 'SuperAdmin entry done successfully.')
                return HttpResponseRedirect('/dashboard/home')
            else:
                user.is_staff = True
                if status == "A":
                    user.is_active = True
                else:
                    user.is_active = False
                user.set_password(password)
                user.save()

                group = Group.objects.get(id=role)
                group.user_set.add(user)
                group.save()
                permission_list = Permission.objects.filter(content_type_id__in=selected_models)
                user.user_permissions.set(permission_list)
                user.save()
                messages.success(request, 'Staff entry done successfully.')
                return HttpResponseRedirect('/dashboard/staff_list')


class EditAdmin(StaffUserOnly, TemplateView):
    template_name = 'dashboard/edit_admin_user.html'

    def get(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        user = User.objects.get(id=user_id)
        user_groups = user.groups.all()
        current_id = 0
        if user_groups:
            current_id = user_groups[0].id
        groups = Group.objects.all()
        model_items = ContentType.objects.exclude(
            app_label__in=["auth", "contenttypes", "sessions", "social_django", "admin"])
        selected_permissions = set(
            Permission.objects.filter(user=user_id).values_list('content_type__model', flat=True).distinct())
        model_list = map(lambda model_item: {"id": model_item.id, "name": model_item.name,
                                             "slug": (model_item.name).replace(" ", "")}, model_items)
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        name = request.POST.get('name')
        last_name = request.POST.get('last_name')
        new_pass = request.POST.get('password')
        role = request.POST.get('role')
        status = request.POST.get('status')
        email = request.POST.get('email')
        selected_models = request.POST.getlist('models[]')
        email = email.lower()

        try:
            user = User.objects.get(id=user_id)
            user.first_name = name
            user.last_name = last_name
            # user.username=email
            user.email = email
            if status == "A":
                user.is_active = True
            else:
                user.is_active = False
            user.save()

            if role == 'super_admin':
                user.is_superuser = True
                user.is_staff = True
                permission_list = Permission.objects.all()
                user.user_permissions.set(permission_list)
                user.save()
                messages.success(request, 'Admin details successfully edited.')
                return HttpResponseRedirect('/dashboard/home')
            else:
                user.is_superuser = False
                user.groups.clear()
                group = Group.objects.get(id=role)
                group.user_set.add(user)
                group.save()
                permission_list = Permission.objects.filter(content_type_id__in=selected_models)
                user.user_permissions.set(permission_list)
                user.save()
                messages.success(request, 'Admin details successfully edited.')
                return HttpResponseRedirect('/dashboard/staff_list')

            if new_pass:
                user.set_password(new_pass)
                user.save()
                messages.success(request, 'Admin details successfully edited.')
                return HttpResponseRedirect('/dashboard/home')

        except Exception as e:
            print(str(e))
            messages.error(request, 'Oops Something went wrong.')
            return HttpResponseRedirect('/dashboard/edit_admin/' + str(user_id))


class DeleteAdmin(StaffUserOnly, View):
    def get(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        try:
            user = User.objects.get(id=user_id)
            user.delete()
            return HttpResponseRedirect('/dashboard/home')
        except Exception as e:
            print(str(e))
            return HttpResponseRedirect('/dashboard/home')


class vechicleListing(StaffUserOnly, View):
	template_name = 'dashboard/vechicle_list.html'
	def get(self, request):
		vechicle = vehicle.objects.all()
		zone = Zone.objects.all()
		url=settings.BASE_URL
		return render(request, self.template_name, locals())

class addVehicle(StaffUserOnly, TemplateView):

	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			current_user = request.user
			user_id = current_user.id
			licenseno = request.POST.get('licenseno')
			zone = request.POST.get('zone')
			vehicleno = request.POST.get('vehicleno')
			manufacturer = request.POST.get('manufacturer')
			
			upfile = request.FILES['file'] if 'file' in request.FILES else False

			
			if(upfile != False):
				now = datetime.datetime.now()

				filename = now.strftime("%f") +'_'+ upfile.name
				print(upfile.name)
				uploaded = settings.MEDIA_ROOT
				print(upfile)
				output_file = open(settings.MEDIA_ROOT+"/vehicle/"+filename,"wb")
				output_file.write(upfile.read())
				output_file.close()
			else:
				filename = ''
			vechicle=vehicle.objects.create(user_id=user_id,image=filename,license=licenseno,zone=zone,vehicleno=vehicleno,manufacturer=manufacturer,created_at=nowTime)
			vechicle.save()
			messages.success(request, 'Vehicle Added successfully.')
			return HttpResponseRedirect('/dashboard/vechicle')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/vechicle')


class vehicleEdit(StaffUserOnly, View):
	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			user_id = kwargs.get('user_id')
			licenseno = request.POST.get('licenseno')
			zone = request.POST.get('zone')
			vehicleno = request.POST.get('vehicleno')
			manufacturer = request.POST.get('manufacturer')
			editid = request.POST.get('editid')
			
			upfile = request.FILES['file'] if 'file' in request.FILES else False

			
			if(upfile != False):
				now = datetime.datetime.now()
				filename = now.strftime("%f") +'_'+ upfile.name
				uploaded = settings.MEDIA_ROOT
				output_file = open(settings.MEDIA_ROOT+"/vehicle/"+filename,"wb")
				output_file.write(upfile.read())
				output_file.close()
				updateValue ={"license":licenseno,"zone":zone,"vehicleno": vehicleno,"manufacturer": manufacturer,"image":filename}
				vechicle=vehicle.objects.filter(id=editid).update(**updateValue)
			else:
				
				updateValue ={"license":licenseno,"zone":zone,"vehicleno": vehicleno,"manufacturer": manufacturer}
				vechicle=vehicle.objects.filter(id=editid).update(**updateValue)
			
			messages.success(request, 'Vehicle Edit successfully.')
			return HttpResponseRedirect('/dashboard/vechicle')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/vechicle')

class vehicleDelete(StaffUserOnly, View):

	def get(self, request, *args, **kwargs):
		 id = kwargs.get('id')

		 try:
			 vechicle = vehicle.objects.get(id=id)
			 vechicle.delete()
			 messages.success(request, 'Vehicle deleted successfully.')
			 return HttpResponseRedirect('/dashboard/vechicle')
		 except Exception as e:
			 print(str(e))
			 return HttpResponseRedirect('/dashboard/vechicle')

class promocodeListing(StaffUserOnly, View):
	template_name = 'dashboard/promocode_list.html'
	def get(self, request):
		promocode = PromoCode.objects.all()
		zone = Zone.objects.all()
		return render(request, self.template_name, locals())

class addPromocode(StaffUserOnly, TemplateView):

	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			current_user = request.user
			user_id = current_user.id
			code_name = request.POST.get('codename')
			amount = request.POST.get('amount')
			start_date = request.POST.get('startdate')
			end_date = request.POST.get('enddate')
			one_time = request.POST.get('onetime')
			zone = request.POST.getlist('zone[]');
			length = len(zone)
			print(length)
			zonedata = ""
			zoneid = ""
			for i in range(length):
				print(zone[i])
				if(zone[i] != ''):
					items = zone[i].split('_')
					zonedata += items[0] + ","
					zoneid += items[1] + ","
			
			zonedata1 = zonedata.rstrip(',')
			zoneid1 = zoneid.rstrip(',')

			
			promocode=PromoCode.objects.create(user_id=user_id,code_name=code_name,zone=zonedata1,zone_id=zoneid1,amount=amount,start_date=start_date,end_date=end_date,one_time=one_time,created_at=nowTime)
			promocode.save()
			messages.success(request, 'PromoCode Added successfully.')
			return HttpResponseRedirect('/dashboard/promocode')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/promocode')



class promocodeEdit(StaffUserOnly, View):
	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			user_id = kwargs.get('user_id')
			code_name = request.POST.get('codename')
			amount = request.POST.get('amount')
			start_date = request.POST.get('startdate')
			end_date = request.POST.get('enddate')
			one_time = request.POST.get('onetime')
			editid = request.POST.get('editid')
			zone = request.POST.getlist('zone[]');
			length = len(zone)
			print(length)
			zonedata = ""
			zoneid = ""
			for i in range(length):
				print(zone[i])
				if(zone[i] != ''):
					items = zone[i].split('_')
					zonedata += items[0] + ","
					zoneid += items[1] + ","
			
			zonedata1 = zonedata.rstrip(',')
			zoneid1 = zoneid.rstrip(',')
			updateValue ={"code_name":code_name,"amount":amount,"start_date": start_date,"end_date": end_date,"one_time": one_time,"zone":zonedata1,"zone_id":zoneid1}
			promocode=PromoCode.objects.filter(id=editid).update(**updateValue)
			messages.success(request, 'PromoCode Edit successfully.')
			return HttpResponseRedirect('/dashboard/promocode')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/promocode')

class promocodeDelete(StaffUserOnly, View):

	def get(self, request, *args, **kwargs):
		 id = kwargs.get('id')

		 try:
			 promocode = PromoCode.objects.get(id=id)
			 promocode.delete()
			 messages.success(request, 'PromoCode Deleted successfully.')
			 return HttpResponseRedirect('/dashboard/promocode')
		 except Exception as e:
			 print(str(e))
			 return HttpResponseRedirect('/dashboard/promocode')

class driverListing(StaffUserOnly, View):
	template_name = 'dashboard/driver_list.html'
	def get(self, request):
		driver = Driver.objects.all()
		url=settings.BASE_URL
		return render(request, self.template_name, locals())




class addDriver(StaffUserOnly, TemplateView):

	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			current_user = request.user
			user_id = current_user.id
			first_name = request.POST.get('firstname')
			last_name = request.POST.get('lastname')
			email = request.POST.get('email')
			phonenumber = request.POST.get('phonenumber')
			stree_address = request.POST.get('streetaddress')
			city = request.POST.get('city')
			state = request.POST.get('state')
			zipcode = request.POST.get('zipcode')
			
			upfile = request.FILES['file'] if 'file' in request.FILES else False

			
			if(upfile != False):
				now = datetime.datetime.now()

				filename = now.strftime("%f") +'_'+ upfile.name
				print(upfile.name)
				uploaded = settings.MEDIA_ROOT
				print(upfile)
				output_file = open(settings.MEDIA_ROOT+"/driver/"+filename,"wb")
				output_file.write(upfile.read())
				output_file.close()
			else:
				filename = ''
			driver=Driver.objects.create(user_id=user_id,image=filename,state=state,zipcode=zipcode,first_name=first_name,last_name=last_name,email=email,phonenumber=phonenumber,stree_address=stree_address,city=city,created_at=nowTime)
			driver.save()
			messages.success(request, 'Driver Added successfully.')
			return HttpResponseRedirect('/dashboard/driver')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/driver')

class driverEdit(StaffUserOnly, View):
	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			user_id = kwargs.get('user_id')
			first_name = request.POST.get('firstname')
			last_name = request.POST.get('lastname')
			email = request.POST.get('email')
			phonenumber = request.POST.get('phonenumber')
			stree_address = request.POST.get('streetaddress')
			city = request.POST.get('city')
			state = request.POST.get('state')
			zipcode = request.POST.get('zipcode')
			editid = request.POST.get('editid')
			upfile = request.FILES['file'] if 'file' in request.FILES else False

			
			if(upfile != False):
				now = datetime.datetime.now()
				filename = now.strftime("%f") +'_'+ upfile.name
				uploaded = settings.MEDIA_ROOT
				output_file = open(settings.MEDIA_ROOT+"/driver/"+filename,"wb")
				output_file.write(upfile.read())
				output_file.close()
				updateValue ={"zipcode":zipcode,"image":filename,"state":state,"first_name":first_name,"last_name":last_name,"email": email,"phonenumber": phonenumber,"stree_address": stree_address,"city":city}
				driver=Driver.objects.filter(id=editid).update(**updateValue)
			else:
				
				updateValue ={"zipcode":zipcode,"state":state,"first_name":first_name,"last_name":last_name,"email": email,"phonenumber": phonenumber,"stree_address": stree_address,"city":city}
				driver=Driver.objects.filter(id=editid).update(**updateValue)
		
			messages.success(request, 'Driver Edit successfully.')
			return HttpResponseRedirect('/dashboard/driver')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/driver')

class driverDelete(StaffUserOnly, View):

	def get(self, request, *args, **kwargs):
		 id = kwargs.get('id')

		 try:
			 driver = Driver.objects.get(id=id)
			 driver.delete()
			 messages.success(request, 'Driver Deleted successfully.')
			 return HttpResponseRedirect('/dashboard/driver')
		 except Exception as e:
			 print(str(e))
			 return HttpResponseRedirect('/dashboard/driver')






class zoneListing(StaffUserOnly, View):
	template_name = 'dashboard/zone_list.html'
	def get(self, request):
		zone = Zone.objects.all()
		return render(request, self.template_name, locals())




class addZone(StaffUserOnly, TemplateView):

	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			current_user = request.user
			user_id = current_user.id
			code_name = request.POST.get('codename')
			name = request.POST.get('name')
			nickname = request.POST.get('nickname')
			flatrateprice = request.POST.get('flatrateprice')
			operationsdays = request.POST.get('operationsdays')
			operationshrs = request.POST.get('operationshrs')

			zone=Zone.objects.create(user_id=user_id,name=name,nickname=nickname,flatrateprice=flatrateprice,operationsdays=operationsdays,operationshrs=operationshrs,created_at=nowTime)
			zone.save()
			messages.success(request, 'Zone Added successfully.')
			return HttpResponseRedirect('/dashboard/zone')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/zone')

class zoneEdit(StaffUserOnly, View):
	def post(self, request, *args, **kwargs):
		try:
			nowTime = datetime.datetime.now()
			user_id = kwargs.get('user_id')
			name = request.POST.get('name')
			nickname = request.POST.get('nickname')
			flatrateprice = request.POST.get('flatrateprice')
			operationsdays = request.POST.get('operationsdays')
			operationshrs = request.POST.get('operationshrs')
			editid = request.POST.get('editid')
			updateValue ={"name":name,"nickname":nickname,"flatrateprice":flatrateprice,"operationsdays":operationsdays,"operationshrs": operationshrs}
			zone=Zone.objects.filter(id=editid).update(**updateValue)
			messages.success(request, 'Zone Edit successfully.')
			return HttpResponseRedirect('/dashboard/zone')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/zone')

class zoneDelete(StaffUserOnly, View):

	def get(self, request, *args, **kwargs):
		 id = kwargs.get('id')

		 try:
			 zone = Zone.objects.get(id=id)
			 zone.delete()
			 messages.success(request, 'Zone deleted successfully.')
			 return HttpResponseRedirect('/dashboard/zone')
		 except Exception as e:
			 print(str(e))
			 return HttpResponseRedirect('/dashboard/zone')
			 
			 
class changePassword(StaffUserOnly, View):
	
	template_name = 'dashboard/change_password.html'
	def get(self, request):
		return render(request, self.template_name)
	
	def post(self, request, *args, **kwargs):
		try:
			
			current_user = request.user
			user_id = current_user.id
			currentpassword= request.user.password
			currentpasswordentered = request.POST.get('oldpassword')
			matchcheck= check_password(currentpasswordentered, currentpassword)
			newpass = request.POST.get('newpassword')
			if matchcheck:
				newpass1 = make_password(newpass)
				updateValue ={"password":newpass1}
				user=User.objects.filter(id=user_id).update(**updateValue)
				messages.success(request, 'Password change successfully.')
				return HttpResponseRedirect('/dashboard/change_password')
			else:
				messages.success(request, 'Old Password wrong.')
				return HttpResponseRedirect('/dashboard/change_password')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/change_password')
	

	
class ManagementUsers(StaffUserOnly, View):
	template_name = 'dashboard/management_users.html'

	def get(self, request):
		current_user = request.user
		user_id = current_user.id
		metausers=User.objects.raw("SELECT * FROM `auth_user` inner join dashboard_metainformaion on auth_user.id=dashboard_metainformaion.user_id")                        

		return render(request, self.template_name, locals())
		
		

class AddUsers(StaffUserOnly,TemplateView):


	def post(self,request,*args, **kwargs):
		nowTime = datetime.datetime.now()
		first_name = request.POST.get('firstname')
		last_name = request.POST.get('lastname')
		username = request.POST.get('username')
		password = request.POST.get("password")
		email = request.POST.get('email')
		phonenumber = request.POST.get('phonenumber')
		city = request.POST.get('city')
		address = request.POST.get('address')
		zipcode = request.POST.get('zipcode')
		state = request.POST.get('state')
		is_staff = 1
		is_active = request.POST.get("status")
		last_login =nowTime
		date_joined =nowTime
		
		try:
			email = User.objects.get(email=email)
			messages.error(request, 'Email Already Exist In Database.')
			return HttpResponseRedirect('/dashboard/user_management')
		except User.DoesNotExist:
			
			try:
				username = User.objects.get(username=username)
				messages.error(request, 'Username Already Exist In Database.')
				return HttpResponseRedirect('/dashboard/user_management')
			except User.DoesNotExist:
				user = User.objects.create_user(
					email=email,
					username=username,
					first_name=first_name,
					last_name=last_name,
					is_staff = is_staff,
					is_active = is_active,
					last_login =last_login,
					date_joined = date_joined,
					is_superuser = 0,
					
				)
				
				user.set_password(password)
				
				user.save()
				user_id = str(user.pk)
				metainformaion=Metainformaion.objects.create(user_id=user_id,phonenumber=phonenumber,city=city,address=address,zipcode=zipcode,state=state)
				metainformaion.save()
				
			messages.success(request, 'User Save successfully.')
			return HttpResponseRedirect('/dashboard/user_management')
			
class editUser(StaffUserOnly,TemplateView):
	def post(self,request,*args, **kwargs):
		nowTime = datetime.datetime.now()
		first_name = request.POST.get('firstname')
		last_name = request.POST.get('lastname')
		username = request.POST.get('username')
		password = request.POST.get("password")
		email = request.POST.get('email')
		phonenumber = request.POST.get('phonenumber')
		city = request.POST.get('city')
		address = request.POST.get('address')
		zipcode = request.POST.get('zipcode')
		editid = request.POST.get('editid')
		state = request.POST.get('state')
		is_staff = 1
		is_active = request.POST.get("status")
		last_login =nowTime
		date_joined =nowTime
		
	
		updateValue1 ={"phonenumber":phonenumber,"city":city,"address":address,"zipcode":zipcode,"state":state}
		metainformaion=Metainformaion.objects.filter(id=editid).update(**updateValue1)

		updateValue ={"email":email,"username":username,"first_name":first_name,"is_staff":is_staff,"is_active": is_active,"last_login":last_login,"date_joined":date_joined}
		user=User.objects.filter(id=editid).update(**updateValue)

		messages.success(request, 'User Edit successfully.')
		return HttpResponseRedirect('/dashboard/user_management')
  
class deleteUser(StaffUserOnly,View):
	def get(self, request, *args, **kwargs):
		user_id = kwargs.get('id')
		try:
			print(user_id)
			user = User.objects.get(id=user_id)
			user.delete()
			metauser = Metainformaion.objects.get(user_id=user_id)
			metauser.delete()
			messages.success(request, 'User deleted succefully.')
			return HttpResponseRedirect('/dashboard/user_management')
		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/user_management')
			
			
class ForgotPassword(TemplateView):
    template_name = 'dashboard/forget_password.html'
    def get(self,request):
        print("5466666666666666666666666666666666666666666666666666666")
        if request.user.is_authenticated:
            return HttpResponseRedirect("/dashboard/home")
        else:
            return render(request,self.template_name,locals())

    def post(self, request, *args, **kwargs):
        email=self.request.POST.get('forget_email')
        try:
            user = User.objects.get(email=email)
            verification_code = randint(100000, 999999)
            forget_pwd_instance = ForgetPassword(forget_user_id=user.id,verification_code=verification_code)
            forget_pwd_instance.save()
            link = settings.BASE_URL+"dashboard/reset_password/"+str(verification_code)
            # content_html = render_to_string("forget_password.html", locals())
            message = "Go to this link for reset you password "+ str(link)
            recipients = [email]
            subject = "Reset Password"
            send_status = send_mail(subject, message, None, recipients, fail_silently=False)
            # send_status = send_mail(subject, recipient_list, message, html_message='')
            if send_status:
                messages.success(request,'Your request has been received.Please check your Email.')
            else:
                messages.error(request,'Some error occur. Retry or contact with administrator.')
        except User.DoesNotExist:
            messages.info(request,'This email address is not registered.')
        except Exception as e:
            raise e
            messages.info(request,'Some error occur. Retry or contact with administrator.')
        return HttpResponseRedirect('/dashboard/forget_password')

class ResetPassword(View):
    template_name = 'dashboard/reset_password.html'
    def get(self, request, *args, **kwargs):
        code=kwargs.get('code')
        return render(request,self.template_name,locals())
    def post(self, request, *args, **kwargs):
        password = request.POST.get("password")
        code=kwargs.get('code')
        try:
            user_instance = ForgetPassword.objects.get(verification_code=code)
            user_instance.forget_user.set_password(password)
            user_instance.forget_user.save()
            messages.success(request,'Password Changed successfully please sign in.')
            return HttpResponseRedirect('/dashboard/login')
        except ForgetPassword.DoesNotExist:
            messages.error(request,'Invaild Email or Code.')
        return HttpResponseRedirect('/dashboard/reset_password/'+code)
		
		

class roleList(StaffUserOnly, TemplateView):


	template_name = "dashboard/role_list.html"
	print('here')
	def get(self, request):
		groups = Group.objects.all()
		return render(request, self.template_name, locals())


class AddRole(StaffUserOnly, TemplateView):
    def post(self,request,*args, **kwargs):
        role = request.POST.get('role')
        try:
            group = Group.objects.get(name=role)
            messages.info(request, "Role already Exist.")
            return HttpResponseRedirect('/dashboard/role')
        except Group.DoesNotExist:
            group = Group.objects.create(name=role)
            group.save()
            messages.info(request, "Role Created Sucessfully")
            return HttpResponseRedirect('/dashboard/role')


class DeleteRole(StaffUserOnly, View):
	def get(self,request,*args, **kwargs):
		group_id = kwargs.get('id')
		try:
			group = Group.objects.get(id=group_id)
			print(group)
			group.delete()
			messages.info(request, "Role Delete Sucessfully")
			return HttpResponseRedirect('/dashboard/role')

		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/role')


class EditRole(StaffUserOnly, View):
	def post(self,request,*args, **kwargs):

		role = request.POST.get('role')
		editid = request.POST.get('editid')

		updateValue ={"name":role}
		group=Group.objects.filter(id=editid).update(**updateValue)
		messages.info(request, "Role Edit Sucessfully")
		return HttpResponseRedirect('/dashboard/role')

class permissionList(StaffUserOnly, TemplateView):


	template_name = "dashboard/permission_list.html"
	def get(self, request):
		groups = Group.objects.all()
		permissions = Permission.objects.all()
		permission=Permission.objects.raw("select GROUP_CONCAT(auth_permission .name) as permission , new.* from (SELECT auth_group.*,dashboard_grouppermission.group_id,dashboard_grouppermission.permission_id FROM `auth_group` inner join dashboard_grouppermission on auth_group.id=dashboard_grouppermission.group_id) as new inner JOIN auth_permission on new.permission_id = auth_permission .id GROUP by group_id")
		return render(request, self.template_name, locals())

class addPermission(StaffUserOnly, TemplateView):
	def post(self,request,*args, **kwargs):

		role = request.POST.get('role')

		permission = request.POST.getlist('permission[]');
		length = len(permission)


		group = Grouppermission.objects.filter(group_id=role).count()
		if(group != 0):
			messages.info(request, "Permission Already Exist.")
			return HttpResponseRedirect('/dashboard/permission')
		else:
			for i in range(length):
				groupdata = Grouppermission.objects.create(group_id=role,permission_id=permission[i])
		messages.info(request, "Permission Added Sucessfully")
		return HttpResponseRedirect('/dashboard/permission')

class deletePermission(StaffUserOnly, View):
	def get(self,request,*args, **kwargs):
		group_id = kwargs.get('id')
		try:
			group = Grouppermission.objects.filter(group_id=group_id)
			group.delete()
			messages.info(request, "Permission Delete Sucessfully")
			return HttpResponseRedirect('/dashboard/permission')

		except Exception as e:
			print(str(e))
			return HttpResponseRedirect('/dashboard/permission')


class editPermission(StaffUserOnly, View):
	def post(self,request,*args, **kwargs):
		role = request.POST.get('role')
		permission = request.POST.getlist('permission[]');
		length = len(permission)
		group = Grouppermission.objects.filter(group_id=role).count()
		if(group != 0):
			group = Grouppermission.objects.filter(group_id=role)
			group.delete()
			for i in range(length):
				groupdata = Grouppermission.objects.create(group_id=role,permission_id=permission[i])
		messages.info(request, "Permission Edited Sucessfully")
		return HttpResponseRedirect('/dashboard/permission')


class Editprofilepic(APIView):
	def post(request,*args):
		base64_data= request.body.decode('utf-8')
		body_data = json.loads(base64_data)
		image=body_data['image']
		user_id=User.object.get(id=user_id)
		print(user_id,"==================")
		save_user_profile=UserProfile.objects.get(user_id=int(request.user.id))
		save_user_profile.image=image
		save_user_profile.save()
		print(save_user_profile,"==++++++=====================")
		return HttpResponse(json.dumps({"success":"1","message":"profile_pic updated successfully"}))

#
# def ride_list(request):
#
#     rides = Ride.objects.all()
#     return render(request, 'dashboard/ridehistory.html', { 'rides': rides })


def ride_list(request):
    rides = Ride.objects.all().filter(user_id=1)
    return render(request, 'dashboard/ridehistory.html', { 'articles': rides })


from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })


def create_session(request):
    request.session['name'] = 'username'
    request.session['password'] = 'password123'
    return HttpResponse("<h1><br> the session is set</h1>")
def access_session(request):
    response = "<h1>Welcome to Sessions </h1><br>"
    if request.session.get('name'):
        response += "Name : {0} <br>".format(request.session.get('name'))
    if request.session.get('password'):
        response += "Password : {0} <br>".format(request.session.get('password'))
        return HttpResponse(response)
    else:
        return redirect('create/')
def delete_session(request):
    try:
        del request.session['name']
        del request.session['password']
    except :
        pass



from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def room(request, room_name):
    return render(request, 'room.html', {
        'room_name': room_name
    })