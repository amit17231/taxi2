from django.db import models
from django.contrib.auth.admin import User


# Create your models here.
class vehicle(models.Model):
    created_at = models.DateField(blank=True, default=None)
    license = models.CharField('License Number', max_length=120, default=None)
    zone = models.CharField('Zone', max_length=255, default=None)
    vehicleno = models.CharField('Vehicle Number', max_length=255, default=None)
    manufacturer = models.CharField('Manufacturer', max_length=255, default=None)
    user_id = models.IntegerField(default=None)
    location = models.CharField('Location', max_length=255, default=None)
    image = models.CharField('image', max_length=255, default=None)


class PromoCode(models.Model):
    code_name = models.CharField(max_length=120, default=None)
    amount = models.CharField(max_length=120, default=None)
    start_date = models.CharField(blank=True, max_length=255, default=None)
    end_date = models.CharField(blank=True, max_length=255, default=None)
    zone = models.CharField(max_length=120, default=None)
    zone_id = models.CharField('Zone id', max_length=120, default=None)
    one_time = models.BooleanField(null=True, blank=True, default=None)
    created_at = models.CharField(blank=True, max_length=255, default=None)
    user_id = models.IntegerField()
    amounttype = models.BooleanField(null=True, blank=True, default=None)


class Driver(models.Model):
    first_name = models.CharField(max_length=120, default=None)
    last_name = models.CharField(max_length=120, default=None)
    email = models.EmailField(max_length=50, default=None)
    password = models.CharField(max_length=120)
    phonenumber = models.CharField(max_length=100, default=None)
    stree_address = models.CharField(max_length=100, default=None)
    city = models.CharField(max_length=255, default=None)
    state = models.CharField(max_length=255, default=None)
    zipcode = models.IntegerField()
    created_at = models.CharField(blank=True, max_length=255, default=None)
    user_id = models.IntegerField(default=None)
    image = models.CharField('image', max_length=255, default=None)
    vehicleno = models.IntegerField(default=None)
    vehicleid = models.CharField('image', max_length=255, default=None)


class Zone(models.Model):
    name = models.CharField('Name', max_length=120, default=None)
    nickname = models.CharField('Nickname', max_length=120, default=None)
    flatrateprice = models.IntegerField()
    operationhrmondaystart = models.CharField('Operation Hours', max_length=120, default=None)
    operationhrmondayend = models.CharField('Operation Hours1', max_length=120, default=None)
    operationhrtuesdaystart = models.CharField('Operation Hours2', max_length=120, default=None)
    operationhrtuesdayend = models.CharField('Operation Hours3', max_length=120, default=None)
    operationhrwednesdaystart = models.CharField('Operation Hours4', max_length=120, default=None)
    operationhrwednesdayend = models.CharField('Operation Hours5', max_length=120, default=None)
    operationhrthursdaystart = models.CharField('Operation Hours6', max_length=120, default=None)
    operationhrthursdayend = models.CharField('Operation Hours7', max_length=120, default=None)
    operationhrfridaystart = models.CharField('Operation Hours8', max_length=120, default=None)
    operationhrfridayend = models.CharField('Operation Hours9', max_length=120, default=None)
    operationhrsaturdaystart = models.CharField('Operation Hours10', max_length=120, default=None)
    operationhrsaturdayend = models.CharField('Operation Hours11', max_length=120, default=None)
    operationhrsundaystart = models.CharField('Operation Hours12', max_length=120, default=None)
    operationhrsundayend = models.CharField('Operation Hours13', max_length=120, default=None)
    operationsdays = models.CharField('Operation Days', max_length=120, default=None)
    lat = models.CharField('lat', max_length=120, default=None)
    lng = models.CharField('lng', max_length=120, default=None)
    created_at = models.CharField(blank=True, max_length=255, default=None)
    user_id = models.IntegerField(default=None)


class Ride(models.Model):
    name = models.CharField('Name', max_length=120, default=None)
    ridestatus = models.CharField('Status', max_length=120, default=None)
    ridearea = models.CharField('Area', max_length=120, default=None)
    ridetime = models.CharField('Ride', max_length=120, default=None)
    created_at = models.CharField(blank=True, max_length=255, default=None)
    user_id = models.IntegerField(default=None)


class Metainformaion(models.Model):
    user_id = models.IntegerField(default=None)
    phonenumber = models.CharField(max_length=100, default=None)
    address = models.CharField(max_length=255, default=None)
    city = models.CharField(max_length=255, default=None)
    image = models.CharField(max_length=255, default=None)
    state = models.CharField(max_length=255, default=None)
    zipcode = models.IntegerField()
    role = models.CharField(max_length=255, default=None)
    role_id = models.IntegerField()


class Grouppermission(models.Model):
    group_id = models.IntegerField()
    permission_id = models.IntegerField()


class ForgetPassword(models.Model):
    forget_user = models.ForeignKey(User, on_delete=models.CASCADE)
    verification_code = models.CharField(max_length=15, blank=True, null=True, default=None)


class Map(models.Model):
    data = models.CharField(max_length=255, default=None)
    map_id = models.IntegerField()


class Zonelocation(models.Model):
    locationname = models.CharField(max_length=255, default=None)
    zoneid = models.IntegerField()


class Tips(models.Model):
    tipname = models.CharField(max_length=255, default=None)
    tipprice = models.CharField(max_length=255, default=None)
    createdby = models.IntegerField()


class Carddetail(models.Model):
    cardholdername = models.TextField(max_length=255, default=None)
    cardnumber = models.CharField(max_length=255, default=None)
    user_id = models.IntegerField(default=None)
    cardvailidupto = models.CharField(max_length=11, default=None)
    cardcvc = models.CharField(max_length=11, default=None)
    cardresponse = models.TextField(max_length=255, default=None)
    address = models.CharField(max_length=255, default=None)
    customertoken = models.CharField(max_length=255, default=None)


class Bookingride(models.Model):
    currentlocation = models.CharField(max_length=255, default=None)
    designation = models.CharField(max_length=255, default=None)
    price = models.CharField(max_length=25, default=None)
    userid = models.CharField(max_length=255, default=None)
    driverid = models.CharField(max_length=255, default=None)
    status = models.CharField(max_length=255, default=None)
    datetime = models.DateTimeField()


class Lostreport(models.Model):
    user_id = models.CharField(max_length=255, default=None)
    types = models.CharField(max_length=255, default=None)
    description = models.CharField(max_length=25, default=None)
    email = models.CharField(max_length=255, default=None)
    phonenumber = models.CharField(max_length=255, default=None)
    dateTime = models.DateTimeField( default=None)


class Contactus(models.Model):
    user_id = models.CharField(max_length=255, default=None)
    name = models.CharField(max_length=255, default=None)
    subject = models.CharField(max_length=25, default=None)
    message = models.CharField(max_length=25, default=None)
    description = models.CharField(max_length=255, default=None)
    email = models.CharField(max_length=255, default=None)
    phonenumber = models.CharField(max_length=255, default=None)
    dateTime = models.DateTimeField( default=None)


class Drivercontact(models.Model):
    user_id = models.CharField(max_length=255, default=None)
    name = models.CharField(max_length=255, default=None)
    status = models.CharField(max_length=255, default=None)
    email = models.EmailField(max_length=255, default=None)
    subject = models.CharField(max_length=255, default=None)
    message = models.CharField(max_length=255, default=None)


class Applyasdriver(models.Model):
    user_id = models.CharField(max_length=255, default=None)
    name = models.CharField(max_length=255, default=None)
    email = models.EmailField(max_length=255, default=None)
    subject = models.CharField(max_length=255, default=None)
    message = models.CharField(max_length=255, default=None)


class Trip(models.Model):
    userid = models.CharField(max_length=255, default=None)
    status = models.CharField(max_length=255, default=None)
    driverid = models.CharField(max_length=255, default=None)
    pickupLocation = models.CharField(max_length=255, default=None)
    designation = models.CharField(max_length=255, default=None)
    tripstartdate = models.DateField( default=None)
    tripstarttime = models.TimeField( default=None)
    tripenddate = models.DateField( default=None)
    tripendtime = models.TimeField( default=None)


class Faqs(models.Model):
    question = models.CharField(max_length=255, default=None)
    answer = models.CharField(max_length=255, default=None)


class Rating(models.Model):
    userid = models.CharField(max_length=255, default=None)
    driverid = models.CharField(max_length=255, default=None)
    rideid = models.CharField(max_length=255, default=None)
    rating = models.CharField(max_length=255, default=None)
    tip = models.CharField(max_length=255, default=None)

