import uuid
from django.utils import timezone
from websocket_server import WebsocketServer
import requests
from django.shortcuts import render
from django.http import HttpResponseRedirect
# from corsheaders.defaults import default_headers
from django.template import Template, Context
from django.http import HttpRequest
from django.http import HttpResponse
from django import http
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.contrib.auth.hashers import make_password
import datetime
import random
import json
import re
from random import *
from dashboard.models import *
from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives, EmailMessage
from requests import Response
from rest_framework.authtoken.views import ObtainAuthToken


from .models import *
from django.core import serializers
from PIL import Image
from io import BytesIO
import  time, base64
from uuid import UUID


from .views import delete_session



class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):

            return obj.hex
        return json.JSONEncoder.default(self, obj)




def appLogin(request):

	login_json_data=json.loads(request.body.decode('utf-8'))
	email = login_json_data['email']
	password = login_json_data['password']
	phonenumber = login_json_data['phonenumber']
	request.session['name'] = 'username'
	request.session['password'] = 'password123'
	rand_token = uuid.uuid4()  # to genrate random token everytime
	key=json.dumps(rand_token, cls=UUIDEncoder)


	if password and email:
		try:
			user = User.objects.get(email=email)

			currentpassword= user.password
			currentpasswordentered = password
			matchcheck= check_password(currentpasswordentered, currentpassword)
			#devicetoken is used to save the ApiToken,devicetoken and User id in db table name DeviceToken
			devicetoken = DeviceToken.objects.update_or_create(api_token=key,user_id=user.id)

			if matchcheck:
				return HttpResponse(json.dumps({'message':'Login successfully with Taxi App.','success':'1','Email':email,"ApiToken":key,
												}))

			else:
				return HttpResponse(json.dumps({'message':'password is wrong.','success':'0'}))



		except User.DoesNotExist:
			return HttpResponse(json.dumps({'message':'Email address is not registered.','success':'1'}))

	if password and phonenumber:
		try:

			user = Metainformaion.objects.get(phonenumber=phonenumber)

			user1 = User.objects.get(id=user.id)
			currentpassword= user1.password
			currentpasswordentered = password
			matchcheck= check_password(currentpasswordentered, currentpassword)

			if matchcheck:
				return HttpResponse(json.dumps({'message':'Login successfully with Taxi App.','success':'1'}))

			else:
				return HttpResponse(json.dumps({'message':'Phonenumber is wrong.','success':'1'}))



		except Metainformaion.DoesNotExist:
			return HttpResponse(json.dumps({'message':'Phone Number is not registered.','success':'1'}))


def appRegistration(request):

	registerJsonData=json.loads(request.body.decode('utf-8'))
	firstname = registerJsonData['firstname']
	lastname = registerJsonData['lastname']
	username = registerJsonData['username']
	password = registerJsonData['password']
	email = registerJsonData['email']
	nowTime = datetime.datetime.now()
	if(firstname == ''):
		return HttpResponse(json.dumps({'message':'Firstname is Missing.','success':0}))
	if(lastname ==''):
		return HttpResponse(json.dumps({'message':'Lastname is Missing.','success':0}))
	if(username ==''):
		return HttpResponse(json.dumps({'message':'Username is Missing.','success':0}))
	if(password ==''):
		return HttpResponse(json.dumps({'message':'password is Missing.','success':0}))
	if(email ==''):
		return HttpResponse(json.dumps({'message':'Email is Missing.','success':0}))
	if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) == '':
		return HttpResponse(json.dumps({'message':'Invalid email.','success':0}))
	try:

		User.objects.get(email=email)
		error ={'message':'User already exists with given email.' ,'success': 0}
		return HttpResponse(json.dumps(error))

	except User.DoesNotExist:
		try:
				User.objects.get(username=username)
				error ={'message':'User already exists with given username.' ,'success': 0}
				return HttpResponse(json.dumps(error))
		except User.DoesNotExist:

				user = User.objects.create_user(
					email=email,
					username=username,
					first_name=firstname,
					last_name=lastname,
					is_staff = 1,
					is_active = 1,
					last_login =nowTime,
					date_joined = nowTime,
					is_superuser = 0,




				)
				user.set_password(password)
				user.save()

				user_id = str(user.pk)

				metainformaion=Metainformaion.objects.create(user_id=user_id,role = 'User', role_id = 14,phonenumber='',city='',address='',zipcode=0,state='')
				metainformaion.save()
	return HttpResponse(json.dumps({'message':'Register successfully with Taxi App.','success':1,"Firstname":firstname,'Lastname':lastname,'Username':username,}))





def changePassword(request):


	changepasswordJsonData=json.loads(request.body)
	oldpassword = changepasswordJsonData['oldpassword']
	password = changepasswordJsonData['password']
	userid = changepasswordJsonData['userid']
	header = custom_headers(request)

	if(oldpassword == ''):
		return HttpResponse(json.dumps({'message':'oldpassword is Missing.','success':0}))
	if(password ==''):
		return HttpResponse(json.dumps({'message':'password is Missing.','success':0}))
	if(userid ==''):
		return HttpResponse(json.dumps({'message':'userid is Missing.','success':0}))

	try:
		user = User.objects.get(id=userid)
		print(user)
		currentpassword= user.password
		currentpasswordentered = oldpassword
		matchcheck= check_password(currentpasswordentered, currentpassword)

		if matchcheck:
			newpass1 = make_password(password)
			updateValue ={"password":newpass1}
			user=User.objects.filter(id=userid).update(**updateValue)
			return HttpResponse(json.dumps({'message':'Password Change Successfully.','success':1,}))
		else:
			return HttpResponse(json.dumps({'message':'Old Password wrong.','success':1}))
	except User.DoesNotExist:
		return HttpResponse(json.dumps({'message':'User ID does not exist in database.','success':1}))

def editProfile(request):
	editprofileJsonData =json.loads(request.body.decode('utf-8'))
	userid = editprofileJsonData['userid']
	firstname= editprofileJsonData['firstname']
	lastname= editprofileJsonData['lastname']
	phonenumber = editprofileJsonData['phonenumber']
	image = editprofileJsonData['image']


	if(firstname == ''):
		return HttpResponse(json.dumps({'message':'Firstname is Missing.','success':0}))
	if(lastname ==''):
		return HttpResponse(json.dumps({'message':'Lastname is Missing.','success':0}))
	if(userid == ''):
		return HttpResponse(json.dumps({'message':'user_id is Missing.','success':0}))
	if(image == ''):
		return HttpResponse(json.dumps({'message':'image is missing','success':0}))

	else:
		try:
			user = User.objects.get(id=userid)
			updateValue ={"last_name":lastname,"first_name":firstname}
			user=User.objects.filter(id=userid).update(**updateValue)
			'''decoding the base64string into image'''
			decodestring = base64.b64decode(image)
			image_data = BytesIO(decodestring)
			img = Image.open(image_data)
			image_name = time.time()
			'''path for save the image'''
			image_path = "/home/amit/Downloads/Taxi2/Rove/media/"
			img.save(image_path + str(image_name) + '.png')
			image=str(image_name)+'.png'
			metainformaion = Metainformaion.objects.filter(user_id=userid).update(phonenumber=phonenumber,image=image)
			return HttpResponse(json.dumps({"success": 1,'message':'Profile Edit Successfully',"Firstname":firstname,"Lastname":lastname,"updated_phonenumber":phonenumber,'updated-image':image}))
		except User.DoesNotExist:
			return HttpResponse(json.dumps({'message':'User ID does not exist in database.','success':0}))


def forgotPassword(request):
	forgotPasswordJsonData=json.loads(request.body)
	email = forgotPasswordJsonData['email']

	if(email ==''):
		return HttpResponse(json.dumps({'message':'Email is Missing.','success':0}))
	if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) == '':
		return HttpResponse(json.dumps({'message':'Invalid email.','success':0}))

	try:
		user = User.objects.get(email=email)
		verification_code = randint(100000, 999999)
		forget_pwd_instance = ForgetPassword(forget_user_id=user.id,verification_code=verification_code)
		forget_pwd_instance.save()
		link = settings.BASE_URL+"resetpassword/"+str(verification_code)

		message = "Go to this link for reset you password "+ str(link)
		recipients = [email]
		subject = "Reset Password"
		send_status = send_mail(subject, message, None, recipients, fail_silently=False)

		if send_status:
			return HttpResponse(json.dumps({'message':'Your request has been received.Please check your Email.','success':1}))
		else:
			return HttpResponse(json.dumps({'message':'Some error occur. Retry or contact with administrator.','success':1}))

	except User.DoesNotExist:
		return HttpResponse(json.dumps({'message':'Email ID does not exist in database.','success':1}))


def resetPassword(request):
	ResetPasswordJsonData=json.loads(request.body)
	password = ResetPasswordJsonData['password']
	code=ResetPasswordJsonData['token']
	try:
		user_instance = ForgetPassword.objects.get(verification_code=code)
		user_instance.forget_user.set_password(password)
		user_instance.forget_user.save()
		return HttpResponse(json.dumps({'message':'Password Changed successfully please sign in.','success':1}))
	except ForgetPassword.DoesNotExist:
		return HttpResponse(json.dumps({'message':'Invaild Token.','success':1}))


def saveMap(request):

	data = request.POST.get('data')
	id = request.POST.get('id')
	mapInfo=Map.objects.all().filter(map_id=id)
	if(data != ''):
		if mapInfo.exists():
			for maplInfos in mapInfo.all():
				records=maplInfos.data
				test=json.loads(records)
				test1=json.loads(data)

				finalList = test1+test
				t=json.dumps(finalList)
				#print(finalList)
				map1=Map.objects.create(data=t,map_id=id)
				map1.save()
		else:
			map1=Map.objects.create(data=data,map_id=id)
			map1.save()
		return HttpResponse(json.dumps({'message':'save.'+data,'success':1}))
	else:
		return HttpResponse(json.dumps({'message':'not save.'+data,'success':0}))
def getMapData(request):
	records = []
	data = request.POST.get('data')

	mapInfo=Map.objects.all().filter(map_id=data)

	if mapInfo.exists():
		for maplInfos in mapInfo.all():

			records=maplInfos.data

		return HttpResponse(json.dumps(records))

def deleteMapShapes(request):
	id = request.POST.get('id')
	if(id != ''):
		map = Map.objects.filter(map_id=id)
		map.delete()
		return HttpResponse(json.dumps({'message':'Shapes delete sucessfully','success':1}))
	else:
		return HttpResponse(json.dumps({'success':0}))



def rideHistory(request):
	rideHistoryJsonData =json.loads(request.body.decode('utf-8'))
	user_id = rideHistoryJsonData['user_id']


	if(user_id == ''):
		return HttpResponse(json.dumps({'message':'user_id is Missing.','success':0}))
	try:
		ridehistory=Ride.objects.all().filter(user_id=user_id)
		result = []
		for data in ridehistory :

			ridehistorydict = {}
			ridehistorydict['name']=data.name
			ridehistorydict['ridearea']=data.ridearea
			ridehistorydict['ridestatus'] =data.ridestatus
			ridehistorydict['ridetime'] =data.ridetime
			ridehistorydict['created_at'] = data.created_at
			result.append(ridehistorydict)
			json.dumps(result)


		return HttpResponse(json.dumps({'message':'success','ridehistory':result}))
	except:
		return HttpResponse(json.dumps({'message':'No history Available','success':1}))





def tip_detail(request):
	editprofileJsonData =json.loads(request.body.decode('utf-8'))
	user_id = editprofileJsonData["user_id"]
	rider_id = editprofileJsonData["rider_id"]
	tip_amount = editprofileJsonData["tip_amount"]
	nowTime = datetime.datetime.now(tz=timezone.utc)

	Tip = Tips.objects.update_or_create(tipprice=tip_amount,rider_id=rider_id, user_id=user_id,date=nowTime)

	if (user_id == ''):
		return HttpResponse(json.dumps({'message':'user_id is missing','success':1}))
	if (rider_id ==''):
		return HttpResponse(json.dumps({'message': 'Rider_id is missing', 'success': 1}))
	if (tip_amount ==''):
		return HttpResponse(json.dumps({'message': 'Tip_amount is missing', 'success': 1}))

	try  :
		'''use for later if we need tip detail in response'''
		tipid=str(Tip.pk)
		# tipdata=tip.objects.filter(pk=tipid)
		# for data in tipdata:
		# 	result=[]
		# 	tipdatainfo={}
		# 	tipdatainfo['tip']=data.tip
		# 	tipdatainfo['rider_id']=data.rider_id
		# 	tipdatainfo['user_id']=data.user_id
		# 	data=json.dumps(tipdatainfo)
		# 	result.append(tipdatainfo)



		return HttpResponse(json.dumps({'message':'Tip successfully paid','success':1}))
	except:

		return HttpResponse(json.dumps({'message':'Paying Tip is failed', 'success':0}))

def driver_detail(request):
	driver_detailJsonData =json.loads(request.body.decode('utf-8'))

	driver_id = driver_detailJsonData["driver_id"]



	if (driver_id == ''):
		return HttpResponse(json.dumps({'message':'Driver_id is missing','success':0}))

	try:
		driverdetail=Driver.objects.filter(user_id=driver_id)
		for data in driverdetail:
			result=[]
			driver_detail_data={}
			driver_detail_data['first_name']=data.first_name
			driver_detail_data['last_name']=data.last_name
			driver_detail_data['phonenumber']=data.phonenumber
			driver_detail_data['city'] = data.city
			driver_detail_data['state'] = data.state
			driverdetail=json.dumps(driver_detail_data)
			result.append(driver_detail_data)

		return HttpResponse(json.dumps({'Driver profile':result,'success':1}))
	except:

		return HttpResponse(json.dumps({'message':'Driver_id is wrong', 'success':0}))



def logout(request):
	logoutJsonData=json.loads(request.body.decode('utf-8'))
	userid=logoutJsonData['userid']

	if(userid==''):
		return HttpResponse(json.dumps({'message':'userid is missing','success':0}))


	# DeviceToken.objects.filter(user_id=userid).delete()
	delete_session(request)

	return HttpResponse(json.dumps({'message': 'you are logout successfully', 'success': 1}))






'''Payment Api'''
def payment(request):
	paymentJsonData=json.loads(request.body.decode('utf-8'))
	user_id=paymentJsonData['user_id']
	rider_id=paymentJsonData['rider_id']
	amount=paymentJsonData['amount']
	stripeemail = paymentJsonData['stripeemail']
	stripetoken = paymentJsonData['stripetoken']
	planprice = paymentJsonData['planprice']
	stripeplanid = paymentJsonData['stripeplanid']
	planstatus = paymentJsonData['planstatus']
	subscriptionid = paymentJsonData['subscriptionid']
	plantype = paymentJsonData['plantype']
	planid=paymentJsonData['planid']
	chargeid=paymentJsonData['chargeid']
	nowTime = datetime.datetime.now(tz=timezone.utc)
	payments = Payments.objects.create(user_id=user_id, rider_id=rider_id, paymentAmount=amount, created_at=nowTime,updated_at=nowTime,stripeemail=stripeemail,
									   stripetoken=stripetoken,planprice=planprice,stripeplanid=stripeplanid,planstatus=planstatus,plantype=plantype,subscriptionid=subscriptionid,planid=planid,chargeid=chargeid)


	if(user_id == ''):
		return HttpResponse(json.dumps({'message':'user_id is Missing.','success':0}))
	if(rider_id == ''):
		return HttpResponse(json.dumps({'message':'rider_id is Missing.','success':0}))
	if(amount == ''):
		return HttpResponse(json.dumps({'message':'amount is Missing.','success':0}))
	if(stripeemail == ''):
		return HttpResponse(json.dumps({'message':'stripeemail is Missing.','success':0}))
	if(stripetoken == ''):
		return HttpResponse(json.dumps({'message':'stripetoken is Missing.','success':0}))
	if(planprice == ''):
		return HttpResponse(json.dumps({'message':'planprice is Missing.','success':0}))
	if(stripeplanid == ''):
		return HttpResponse(json.dumps({'message':'stripeplanid is Missing.','success':0}))
	if(subscriptionid == ''):
		return HttpResponse(json.dumps({'message':'subscriptionid is Missing.','success':0}))
	if(planstatus == ''):
		return HttpResponse(json.dumps({'message': 'planstatus is missing','success':0}))
	if(plantype == ''):
		return HttpResponse(json.dumps({'message': 'plantype is missing','success':0}))
	if(planid == ''):
		return HttpResponse(json.dumps({'message': 'planid is missing','success':0}))
	if(chargeid == ''):
		return HttpResponse(json.dumps({'message': 'chargeid is missing','success':0}))
	try:
		paymentid = str(payments.pk)
		paymentdata=Payments.objects.filter(pk=paymentid)

		for data in paymentdata:
			result=[]
			paymentdatainfo={}
			paymentdatainfo['user_id']=data.user_id
			paymentdatainfo['rider_id']=data.rider_id
			paymentdatainfo['paymentAmount']=data.paymentAmount
			paymentdatainfo['stripeemail']=data.stripeemail
			paymentdatainfo['stripeplanid']=data.stripeplanid

			json.dumps(paymentdatainfo)
			result.append(paymentdatainfo)



		return HttpResponse(json.dumps({'message':'Payment is done','Payment Detail':result,'payment_id':paymentid,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'Payment is failed','success':0}))





def custom_headers(request):
	tokenauthJsonData = json.loads(request.body.decode('utf-8'))
	Devicetoken = tokenauthJsonData["Devicetoken"]
	AppVersion = tokenauthJsonData["AppVersion"]
	DeviceType = tokenauthJsonData["DeviceType"]

	if(Devicetoken ==''):
		return HttpResponse(json.dumps({'message':'Devicetoken is Missing.','success':0}))
	if(AppVersion ==''):
			return HttpResponse(json.dumps({'message':'AppVersion is Missing.','success':0}))
	if(DeviceType ==''):
		return HttpResponse(json.dumps({'message':'DeviceType is Missing.','success':0}))
	headers = {'AppVersion': '{0}'.format(AppVersion),
			   'DeviceToken ': '{0}'.format(Devicetoken),
			   'DeviceType' :'{0}'.format(DeviceType),

			   }
	return (headers)


def CardSave(request):
	CardSaveJsonData = json.loads(request.body.decode('utf-8'))
	CardHolderName = CardSaveJsonData["CardHolderName"]
	CardNumber = CardSaveJsonData["CardNumber"]
	CardVailidUpto = CardSaveJsonData["CardVailidUpto"]
	Card_CVC=CardSaveJsonData["Card_CVC"]
	CardResponse=CardSaveJsonData["CardResponse"]
	Address = CardSaveJsonData["Address"]  #For Saving The Address of CardHolder
	""""validation for CardResponse for empty input is not yet set because not know about this field"""
	if(CardHolderName ==''):
		return HttpResponse(json.dumps({'message':'CardHolderName is Missing.','success':0}))
	if(CardNumber ==''):
			return HttpResponse(json.dumps({'message':'CardNumber is Missing.','success':0}))
	if(CardVailidUpto ==''):
		return HttpResponse(json.dumps({'message':'CardVailidUpto is Missing.','success':0}))
	if(Card_CVC ==''):
		return HttpResponse(json.dumps({'message':'Card_CVC is Missing.','success':0}))

	try:

		"""For saving CardDetail in Db tablename CardDetail"""
		carddetail = CardDetail.objects.update_or_create(CardHolderName=CardHolderName,CardNumber=CardNumber,CardVailidUpto=CardVailidUpto,
														 Card_CVC=Card_CVC,	CardResponse=CardResponse,Address=Address)
		return HttpResponse(json.dumps({'message':'Card Detail Saved Successfully','Success':1}))
	except:
		return HttpResponse(json.dumps({'message': 'Card Detail not Saved','Success':0}))



def intialstatebooking(request):
	intialstatebookingJsonData = json.loads(request.body.decode('utf-8'))
	currentLocation =intialstatebookingJsonData["currentLocation"]
	designation = intialstatebookingJsonData["designation"]
	price = intialstatebookingJsonData["price"]
	userid=intialstatebookingJsonData["userid"]

	nowTime = datetime.datetime.now(tz=timezone.utc)


	if(currentLocation ==''):
		return HttpResponse(json.dumps({'message':'currentLocation is Missing.','success':0}))
	if(designation ==''):
			return HttpResponse(json.dumps({'message':'designation is Missing.','success':0}))
	if(price ==''):
		return HttpResponse(json.dumps({'message':'price is Missing.','success':0}))
	if(userid ==''):
		return HttpResponse(json.dumps({'message':'userid is Missing.','success':0}))


	try:

		"""For saving BookingDetail in Db tablename Booking"""
		initialbooking = Bookingride.objects.create(currentLocation=currentLocation,designation=designation,price=price,
														 userid=userid,	status="initial",dateTime=nowTime,)


		bookingid=str(initialbooking.pk)


		return HttpResponse(json.dumps({'message':'Booking in intial state Successfully Done','success':1,"CurrentLocation":currentLocation,
										"Designation":designation,"price":price,"userid":userid,"status":"initial","BookingId":bookingid}))
	except:
		return HttpResponse(json.dumps({'message': 'Booking in intial state not complete','success':0}))


def Startstatebooking(request):
	intialstatebookingJsonData = json.loads(request.body.decode('utf-8'))
	bookingid=intialstatebookingJsonData['bookingid']

	if(bookingid ==''):
		return HttpResponse(json.dumps({'message':'bookingid is Missing.','success':0}))

	try:
		''' to get the information  after status change of related bookingId '''
		StartState= Bookingride.objects.filter(pk=bookingid).update(status="start")
		bookingdata = Bookingride.objects.all().filter(pk=bookingid)
		result = []
		for data in bookingdata:
			bookingdatadict = {}
			bookingdatadict["currentLocation"] = data.currentLocation
			bookingdatadict["designation"] = data.designation
			bookingdatadict["price"] = data.price
			bookingdatadict["userid"] = data.userid
			bookingdatadict["status"] = data.status


			result.append(bookingdatadict)
			data = json.dumps(result)
		return HttpResponse(json.dumps({'message':'success','information':result,'success':1}))


	except:
		return HttpResponse(json.dumps({'message':'Booking in not in start state','success':0}))

'''CancelBooking Api'''

def Cancelbooking(request):
	CancelbookingJsonData = json.loads(request.body.decode('utf-8'))
	bookingid=CancelbookingJsonData['bookingid']

	if(bookingid ==''):
		return HttpResponse(json.dumps({'message':'bookingid is Missing.','success':0}))

	try:
		CancelState= Bookingride.objects.filter(pk=bookingid).update(status="Canceled")
		"""to get the information  after status change of related bookingId"""

		bookingdata = Bookingride.objects.all().filter(pk=bookingid)
		result = []
		for data in bookingdata:
			extractedbookingdata = {}
			extractedbookingdata["currentLocation"] = data.currentLocation
			extractedbookingdata["designation"] = data.designation
			extractedbookingdata["price"] = data.price
			extractedbookingdata["userid"] = data.userid
			extractedbookingdata["status"] = data.status

			result.append(extractedbookingdata)
			data = json.dumps(result)

		return HttpResponse(json.dumps({'message':'Booking is canceled','information':result,'success':1}))


	except:
		return HttpResponse(json.dumps({'message':'There is an error in cancel booking','success':0}))

"""LostReport Api"""

def lostreport(request):
	LostReportJsonData = json.loads(request.body.decode('utf-8'))
	user_id =LostReportJsonData["user_id"]
	types = LostReportJsonData["types"]
	description = LostReportJsonData["description"]
	email=LostReportJsonData["email"]
	phonenumber = LostReportJsonData["phonenumber"]
	nowTime = datetime.datetime.now(tz=timezone.utc)



	if(user_id ==''):
		return HttpResponse(json.dumps({'message':'user_id is Missing.','success':0}))
	if(types ==''):
			return HttpResponse(json.dumps({'message':'types is Missing.','success':0}))
	if(description ==''):
		return HttpResponse(json.dumps({'message':'description is Missing.','success':0}))
	if(phonenumber==''):
		return HttpResponse(json.dumps({'message':'phonenumber is Missing.','success':0}))
	if (email==''):
		return HttpResponse(json.dumps({'message': 'email is Missing.', 'success': 0}))

	try:
		lostdetail = Lostreport.objects.create(user_id =user_id ,types=types,description=description,phonenumber=phonenumber,
														   email=email,dateTime=nowTime)
		reportid=  str(lostdetail.pk)

		return HttpResponse(json.dumps({'message':'Lost Report is registered','user_id':user_id,'types':types,'phonenumber':phonenumber,'email':email,"reportid":reportid,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'Report registration failed','success':0}))


""" Contact Us Api"""

def Contactus(request):
	contactusJsonData=json.loads(request.body.decode('utf-8'))
	name = contactusJsonData["name"]
	subject = contactusJsonData["subject"]
	message = contactusJsonData["message"]
	email = contactusJsonData["email"]
	phonenumber = contactusJsonData["phonenumber"]
	description = contactusJsonData["description"]
	nowTime = datetime.datetime.now(tz=timezone.utc)

	recipients = [settings.EMAIL_HOST_USER]

	send_status = send_mail(subject, message, email, recipients, fail_silently=False)

	if(name ==''):
			return HttpResponse(json.dumps({'message':'name is Missing.','success':0}))
	if(message ==''):
		return HttpResponse(json.dumps({'message':'message is Missing.','success':0}))
	if(subject==''):
		return HttpResponse(json.dumps({'message':'subject is Missing.','success':0}))
	if (email==''):
		return HttpResponse(json.dumps({'message': 'email is Missing.', 'success': 0}))

	if (description==''):
		return HttpResponse(json.dumps({'message': 'description is Missing.', 'success': 0}))

	try:
		"""conactus is our db table where we are storing the information"""
		Contactus.objects.create(name =name, subject =subject, message =message, phonenumber =phonenumber, email =email, dateTime =nowTime)

		return HttpResponse(json.dumps({"message":"Our CustomerCare Contact with You Soon","success":1}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur in ContactUs','success':"False"}))

"""DriverContact with admin API"""

def driverContact(request):
	driverContactJsonData=json.loads(request.body.decode('utf-8'))
	status = driverContactJsonData["status"]
	subject = driverContactJsonData["subject"]
	message = driverContactJsonData["message"]
	email = driverContactJsonData["email"]
	nowTime = datetime.datetime.now(tz=timezone.utc)

	recipients = [settings.EMAIL_HOST_USER]
	'''for sending email'''
	send_status = send_mail(subject, message, email, recipients, fail_silently=False)


	if(status ==''):
			return HttpResponse(json.dumps({'message':'status is Missing.','success':0}))
	if(message ==''):
		return HttpResponse(json.dumps({'message':'message is Missing.','success':0}))
	if(subject==''):
		return HttpResponse(json.dumps({'message':'subject is Missing.','success':0}))
	if (email==''):
		return HttpResponse(json.dumps({'message': 'email is Missing.', 'success': 0}))

	try:
		"""Drivercontact is our db table where we are storing the information"""
		Drivercontact.objects.create(status =status, subject =subject, message =message, email =email)

		return HttpResponse(json.dumps({"message":"Our CustomerCare Contact with You Soon","success":1}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur ','success':0}))



"""Rating Api"""

def rating(request):
	ratingJsonData=json.loads(request.body.decode('utf-8'))
	userid =ratingJsonData["userid"]
	driverid = ratingJsonData["driverid"]
	rating = ratingJsonData["rating"]
	rideid = ratingJsonData["rideid"]
	tip=ratingJsonData["tip"]
	nowTime = datetime.datetime.now(tz=timezone.utc)


	if(userid == ''):
		return HttpResponse(json.dumps({'message':'userid is Missing.','success':0}))
	if(driverid == ''):
		return HttpResponse(json.dumps({'message':'driverid is Missing.','success':0}))
	if(rideid  == ''):
		return HttpResponse(json.dumps({'message':'rideid is Missing.','success':0}))
	if(rating == ''):
		return HttpResponse(json.dumps({'message':'rating is Missing.','success':0}))


	try:
		"""Rating is our db table where we are storing the information"""
		Rating.objects.create(userid =userid, driverid =driverid,rideid =rideid,rating =rating,tip=tip)

		return HttpResponse(json.dumps({"message":"Rating is done successfully","success":1,"rating":rating,
										"tip":tip}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur ','success':0}))

"""Promocode API"""

def promocode(request):
	promocodeJsonData=json.loads(request.body.decode('utf-8'))
	title =promocodeJsonData["title"]
	code = promocodeJsonData["code"]
	start_date =promocodeJsonData["start_date"]
	end_date = promocodeJsonData["end_date"]

	nowTime = datetime.datetime.now(tz=timezone.utc)


	if(title == ''):
		return HttpResponse(json.dumps({'message':'title is Missing.','success':0}))
	if(code == ''):
		return HttpResponse(json.dumps({'message':'code is Missing.','success':0}))
	if(start_date == ''):
		return HttpResponse(json.dumps({'message':'start_date is Missing.','success':0}))
	if(end_date == ''):
		return HttpResponse(json.dumps({'message':'end_date is Missing.','success':0}))



	try:
		"""Promocodes is our db table where we are storing the information"""
		PromoCode.objects.create(title =title, code =code,start_date =start_date,end_date =end_date,created_at =nowTime)

		return HttpResponse(json.dumps({"message":"Promocode is created successfully","success":1,"Title":title,"Promocode":code,
										"start_date":start_date,"end_date":end_date}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur ','success':0}))


"""FAQs Api"""
def FAqs(request):

	FAQsdata=Faqs.objects.all()
	result = []
	for data in FAQsdata:
		FAQsdata1 = {}
		FAQsdata1["Question"] = data.Question
		FAQsdata1["Answer"] = data.Answer


		result.append(FAQsdata1)
		Data = json.dumps(result)

	return HttpResponse(json.dumps({'message': 'success','FAQs':result,'success':1}))
'''go_online driver's api'''
def go_online(request):
	go_onlineJsonData=json.loads(request.body.decode('utf-8'))
	userid=go_onlineJsonData['userid']

	if(userid ==''):
		return HttpResponse(json.dumps({'message': 'userid is Missing.', 'success': 0}))

	try:


		Driver.objects.filter(user_id=userid).update(status="online")
		return HttpResponse(json.dumps({'message':'you are online now','user_id':userid,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'Some Error occur while going online','success':0}))

'''go_offline driver's api'''
def go_offline(request):
	go_offlineJsonData=json.loads(request.body.decode('utf-8'))
	userid=go_offlineJsonData['userid']

	if(userid ==''):
		return HttpResponse(json.dumps({'message': 'userid is Missing.', 'success': 0}))

	try:

		Driver.objects.filter(user_id=userid).update(status="offline")
		return HttpResponse(json.dumps({'message':'you are offline now','success':1}))
	except:
		return HttpResponse(json.dumps({'message':'Some Error occur while going offline','success':0}))



'''Found Lost Item Api'''
def FoundLostItem(request):
	FoundLostItemJsonData = json.loads(request.body.decode('utf-8'))

	item_types = FoundLostItemJsonData["item_types"]
	description = FoundLostItemJsonData["description"]
	passenger_name=FoundLostItemJsonData["passenger_name"]
	email=FoundLostItemJsonData["email"]
	phonenumber = FoundLostItemJsonData["phonenumber"]
	nowTime = datetime.datetime.now(tz=timezone.utc)




	if(item_types ==''):
			return HttpResponse(json.dumps({'message':'item type is Missing.','success':0}))
	if(description ==''):
		return HttpResponse(json.dumps({'message':'description is Missing.','success':0}))
	if(phonenumber==''):
		return HttpResponse(json.dumps({'message':'phonenumber is Missing.','success':0}))
	if (email==''):
		return HttpResponse(json.dumps({'message': 'email is Missing.', 'success': 0}))
	if (passenger_name==''):
		return HttpResponse(json.dumps({'message': 'passenger_name is Missing.', 'success': 0}))

	try:
		Founditemdetail = Foundlostitem.objects.create(item_types=item_types,description=description,passenger_name=passenger_name,phonenumber=phonenumber,
														   email=email,dateTime=nowTime)

		Founditemdetailid= str(Founditemdetail.pk)
		return HttpResponse(json.dumps({'message':'FoundedLostitenm  is registered','Item_types':item_types,'passenger_name':passenger_name,'phonenumber':phonenumber,'email':email,'FoundItemId':Founditemdetailid,"succes":1}))
	except:
		return HttpResponse(json.dumps({'message':'Report registration failed','success':0}))


'''driverpastrides API'''
def driverpastrides(request):
	driverpastridesJsonData =json.loads(request.body.decode('utf-8'))
	userid = driverpastridesJsonData['userid']


	if(userid == ''):
		return HttpResponse(json.dumps({'message':'userid is Missing.','success':0}))
	try:
		ridehistory=Trip.objects.all().filter(driverid=userid)

		Ridehistoryresult = []
		for data in ridehistory :

			ridehistoryDict = {}
			ridehistoryDict['pickuplocation']=data.pickupLocation
			ridehistoryDict['designation']=data.designation
			ridehistoryDict['status'] =data.status
			ridehistoryDict['price'] = data.price

			Ridehistoryresult.append(ridehistoryDict)
			json.dumps(Ridehistoryresult)



		return HttpResponse(json.dumps({'message':'success','Past Rides':Ridehistoryresult,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'No history Available','success':0}))

'''Users ridehistory Api'''
def ridehisrory(request):
	driverpastridesJsonData =json.loads(request.body.decode('utf-8'))
	userid = driverpastridesJsonData['userid']


	if(userid == ''):
		return HttpResponse(json.dumps({'message':'userid is Missing.','success':0}))
	try:
		ridehistory=Trip.objects.all().filter(userid=userid)

		Ridehistoryresult = []
		for data in ridehistory :

			ridehistoryDict = {}
			ridehistoryDict['pickuplocation']=data.pickupLocation
			ridehistoryDict['designation']=data.designation
			ridehistoryDict['status'] =data.status
			ridehistoryDict['price'] = data.price
			ridehistoryDict['status'] = data.status
			ridehistoryDict['startdate'] = data.startdate
			ridehistoryDict['starttime'] = data.starttime
			ridehistoryDict['endtime'] =data.endtime


			Ridehistoryresult.append(ridehistoryDict)
			json.dumps(Ridehistoryresult)



		return HttpResponse(json.dumps({'message':'success','Past Rides':Ridehistoryresult,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'No history Available','success':0}))




def Sendrequest(request):
	SendrequestJsonData=json.loads(request.body.decode('utf-8'))
	userid=SendrequestJsonData['userid']
	bookingid=SendrequestJsonData['bookingid']
	driverid=SendrequestJsonData['driverid']

	if(bookingid ==''):
		return HttpResponse(json.dumps({'message':'bookingid is Missing.','success':0}))

	if(userid==''):
		return HttpResponse(json.dumps({'message':'userid is missing','success':0}))

	if(driverid==''):
		return HttpResponse(json.dumps({'message':'driverid is missing','success':0}))




	try:

		'''for getting the name of user who make request for ride'''
		userdata=User.objects.all().filter(pk=userid)
		UserName = []
		for username in userdata :
			usernameDict = {}
			usernameDict=username.first_name

			UserName.append(usernameDict)
			json.dumps(UserName)


			'''For updating the driverid in booking'''
			Bookingride.objects.filter(pk=bookingid).update(driverid=driverid)


		return HttpResponse(json.dumps({'message':'you have a request for ride ',"Name":UserName,'success':1}))
	except:

		return HttpResponse(json.dumps({'message':'There is an error occur ','success':0}))


def server1():
	def new_client(client, server):
		msg = 'hello'
		print("New client connected and was given id %d" % client['id'])
		print('sent message >' + msg)
		# server.send_message_to_all(msg)
		server.send_message(client, msg)

	# Called for every client disconnecting
	def client_left(client, server):
		print("Client(%d) disconnected" % client['id'])

	# Called when a client sends a message
	def message_received(client, server, message):
		if len(message) > 200:
			message = message[:200] + '..'
		print("Client(%d) said: %s" % (client['id'], message))

		server.shutdown()

	PORT = 9001
	server = WebsocketServer(PORT)
	server.set_fn_new_client(new_client)


	server.set_fn_client_left(client_left)
	server.set_fn_message_received(message_received)

	server.run_forever()






'''Api for available driver's list'''
def driverlist(request):

	try:
		'''To get detail of drivers'''
		Driverlist = Driver.objects.all()

		driverlisting = []
		for data in Driverlist:
			driverdataDict = {}
			driverdataDict['user_id'] = data.user_id
			driverdataDict['first_name'] = data.first_name
			driverdataDict['last_name'] = data.last_name
			driverdataDict['currentlocation'] = data.currentlocation
			driverdataDict['email'] = data.email
			driverdataDict['phonenumber'] = data.phonenumber
			driverdataDict['street_address'] = data.street_address
			driverdataDict['city'] = data.city
			driverdataDict['state'] = data.state



			driverlisting.append(driverdataDict)
			json.dumps(driverlisting)

		return HttpResponse(json.dumps({'message':'success','Driver_List':driverlisting,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'No Driver Exist','success':0}))





'''AcceptRide Api'''
def acceptride(request):
	acceptrideJsondata=json.loads(request.body.decode('utf-8'))
	userid=acceptrideJsondata['userid']
	bookingid=acceptrideJsondata['bookingid']

	if(bookingid ==''):
		return HttpResponse(json.dumps({'message':'bookingid is Missing.','success':0}))

	if(userid==''):
		return HttpResponse(json.dumps({'message':'userid is missing','success':0}))

	try:
		'''for geting the name of pickup user'''
		data=User.objects.all().filter(pk=userid)
		result = []
		for username in data :
			usernameDict = {}
			usernameDict=username.first_name

			result.append(usernameDict)
			json.dumps(result)
			'''for changing the status of booking from initial to start'''
			Bookingride.objects.filter(pk=bookingid).update(status="start")



		return HttpResponse(json.dumps({'message':'Ride is accepted','Pickup':result,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur','Success':0}))

'''Api for Reject the ride by driver'''
def rejectride(request):
	rejectrideJsondata=json.loads(request.body.decode('utf-8'))
	userid=rejectrideJsondata['userid']
	bookingid=rejectrideJsondata['bookingid']
	'''Validation on fields'''
	if(bookingid ==''):
		return HttpResponse(json.dumps({'message':'bookingid is Missing.','success':0}))

	if(userid==''):
		return HttpResponse(json.dumps({'message':'userid is missing','success':0}))

	try:
		'''updating the Booking status when driver rejects ridr request'''
		Bookingride.objects.filter(pk=bookingid).update(status="Cancelled")



		return HttpResponse(json.dumps({'message':'Ride request is Rejected','success':1}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur','success':0}))



def addtaxinumber(request):
	addtaxiJsonData=json.loads(request.body.decode('utf-8'))
	vehicleno=addtaxiJsonData['vehicleno']
	userid=addtaxiJsonData['userid']
	'''Validation on fields'''
	if(userid==''):
		return HttpResponse(json.dumps({'message':'userid is missing','success':0}))

	if(vehicleno==''):
		return HttpResponse(json.dumps({'message':'vehicleno is missing','success':0}))
	try:
		'''To get the vehicleno from db '''
		vehicledata = vehicle.objects.get(vehicleno=vehicleno)

		existingvehicleno = vehicledata.vehicleno
		vehiclenoentered = vehicleno

		'''for checking the vehicle no in db'''
		if(existingvehicleno==vehiclenoentered):
			'''updating the vehicleno in driver table'''
			Driver.objects.filter(user_id=userid).update(vehicleno=vehicleno)
			return HttpResponse(json.dumps({'message':'vehicle is added successfully','success':1}))
		else:
			return HttpResponse(json.dumps({'message': 'vehicle is  not added', 'success': 0}))
	except:
		return HttpResponse(json.dumps({'message':'vehicleno is not existing in database','success':0}))

'''Starttrip Api'''
def starttrip(request):
	starttripJsonData=json.loads(request.body.decode('utf-8'))
	bookingid=starttripJsonData['bookingid']
	nowdate =datetime.date.today()
	nowtime = datetime.datetime.now().time()
	'''Validation on field'''
	if(bookingid==''):
		return HttpResponse(json.dumps({'message':'booking id is missing','success':0}))
	try:
		'''For getting the detail for trip from booking '''
		bookingiddetail=Bookingride.objects.get(pk=bookingid)
		userid=bookingiddetail.userid
		driverid=bookingiddetail.driverid
		pickupLocation=bookingiddetail.currentLocation
		designation=bookingiddetail.designation
		price=bookingiddetail.price

		Tripdetail=Trip.objects.create(userid=userid,driverid=driverid,pickupLocation=pickupLocation,designation=designation,status='start',startdate=nowdate,starttime=nowtime,price=price)
		triptid = str(Tripdetail.pk)

		return HttpResponse(json.dumps({'message':'Trip is started','success':1,'tripid':triptid}))
	except:
		return HttpResponse(json.dumps({'message':'starting trip is failed','success':0}))



'''Endtrip Api'''
def endtrip(request):
	endtripJsonData=json.loads(request.body.decode('utf-8'))
	tripid=endtripJsonData['tripid']
	nowtime = datetime.datetime.now().time()
	'''Validation on field'''
	if(tripid==''):
		return HttpResponse(json.dumps({'message':'booking id is missing','success':0}))
	try:
		Trip.objects.filter(pk=tripid).update(status="completed",endtime=nowtime)

		return HttpResponse(json.dumps({'message':'Trip is completed','success':1}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur please try again','success':0}))


'''message APi'''
def messagesApi(request):
	messagesJsonData=json.loads(request.body.decode('utf-8'))
	senderid=messagesJsonData['senderid']
	receiverid=messagesJsonData['receiverid']
	message=messagesJsonData['message']
	nowdate =datetime.date.today()
	nowtime = datetime.datetime.now().time()

	'''Validation on fields'''
	if(senderid==''):
		return HttpResponse(json.dumps({'message':'senderid is missing','success':0}))
	if(receiverid==''):
		return HttpResponse(json.dumps({'message':'receiverid is missing','success':0}))
	if(message==''):
		return HttpResponse(json.dumps({'message':'message is missing','success':0}))

	try:
		messages.objects.create(senderid=senderid,receiverid=receiverid,message=message,date=nowdate,time=nowtime)
		return HttpResponse(json.dumps({'message':'message sent','success':1}))
	except:
		return HttpResponse(json.dumps({'message':'message is not sent','success':0}))


'''storing driver's locations latlng Api'''
def locationLatLong(request):
	locationlatlongJsonData=json.loads(request.body.decode('utf-8'))
	userid=locationlatlongJsonData['userid']
	location=locationlatlongJsonData['location']
	'''Validation on fields'''
	if(userid==''):
		return HttpResponse(json.dumps({'message':'userid is missing','success':0}))

	if(location ==''):
			return HttpResponse(json.dumps({'message':'location is Missing.','success':0}))



	google_geocode_key = 'AIzaSyAVvDxJM1uepF2cBfFn4D7AzMAZFwObnbY'

	url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + "'" + location + "'" + '&key=' + google_geocode_key


	response = requests.get(url)
	Array = response.json()
	'''acessing the Latitude&longitude from json response'''
	latlng = Array['results'][0]['geometry']['location']

	''' saving Latitude&longitude in db '''
	Driver.objects.filter(pk=userid).update( currentlocation=latlng)

	try:

		return HttpResponse(json.dumps({'message':'success','Location':location,'Latitude&longitude':latlng,'success':1}))
	except:
		return HttpResponse(json.dumps({'message':'There is an error occur','success':0,}))
































